const vscode = require('vscode')

function activate(context) {
	console.log('Gitloot on the road !')
	let disposable = vscode.commands.registerCommand('gitloot.poush', function () {
		const terminals = vscode.window.terminals
		if(terminals.length === 0) return vscode.window.showErrorMessage("Gitloot - No terminal.")
		var terminal = terminals[0]
		vscode.window.showInputBox(vscode.window.createInputBox()).then((message) => {
			if(message) {
				vscode.window.showInformationMessage('Gitloot - Trying to push...')
				terminal.sendText("git add .",true)
				terminal.sendText(`git commit -m "${message}"`,true)
				terminal.sendText("git push -u origin master",true)
			} else return vscode.window.showErrorMessage("Gitloot - Commit message is empty.")
		})
	})
	context.subscriptions.push(disposable)
}

function deactivate() {}

module.exports = {
	activate,
	deactivate                                                                                                                                                                                                                                                                                                                                                                                       
}
